FactoryGirl.define do
  factory :country do
    name "MyString"
    factory :flag do
      image Rack::Test::UploadedFile.new("#{Rails.root}/spec/features/images/missing-image.png", "image/png")
    end
    indicatif 1
  end

end
