if Rails.env.development? || Rails.env.test?
  require "factory_girl"

  namespace :dev do
    desc "Seed data for development environment"
    task prime: "db:setup" do
      include FactoryGirl::Syntax::Methods

      # create(:user, email: "user@example.com", password: "password")
      make_countries
    end
  end

  def make_countries
    countries = [['France', 33], ['Niger', 227], ['Espagne', 34], ['Portugal', 351], ['Angleterre', 44]]
    countries.map do |name, indicatif|
      create(:country, _id: name, name: name, indicatif: indicatif)
    end
  end
end
