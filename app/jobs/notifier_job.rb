class NotifierJob < ActiveJob::Base
  queue_as :default

  def perform
    Rails.logger.info "\n\n\n= = = = = = Send email to all users = = = = = = \n\n\n"
    Rails.logger.info "Send email to all users"
    User.each do |user|
      Rails.logger.info "mailto: => #{user.email}"
      @user = user
      NotifierMailer.send_email(@user).deliver_later
    end
    Rails.logger.info "\n\n\n # # # # # End sending email # # # # # #\n\n\n"
  end

end
