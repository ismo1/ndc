class NotifierMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.notifier_mailer.send_email.subject
  #
  def send_email(user)
    @greeting = "Hi"
    @user = user
    mail(to: @user.email, subject: 'Send email from plateforme.cfeb-france.org!')
  end

  def send_identification_confirmation(user)
    @user = user
    mail(to: @user.email, subject: "Identification confirmation")
  end

  # def send_identification_confirmation_to_all_confirmed
  # 	User.confirmed.each do |user|
  # 		NotifierMailer.send_identification_confirmation(user).deliver
  # 	end
  # end

  # def send_ask_confirmation_to_all_unconfirmed
  # 	User.unconfirmed.each do |user|
  # 		user.send_confirmation_instructions
  # 	end
  # end

  #def send_sensus_information_notification_to_all_confirmed
  #	User.confirmed.each do |user|
  #		NotifierMailer.send_sensus_information_notification(user).deliver
  #	end
  #end




  def send_sensus_information_notification(user)
    @user = user
    mail(to: @user.email, subject: "Les sites retenus pour le recencement")
  end

  def send_notification(user, object= "notification", msg_head="Hi Admin!", msg_body="", msg_footer="Thanks,")
    @msg_head = msg_head
    @msg_body = msg_body
    @msg_footer = msg_footer
    @user = user
    mail(to: @user.email, subject: object)
  end

  def notify_admin(object= "notification", msg_head="Hi Admin!", msg_body="", msg_footer="Thanks,")
    @msg_head = msg_head
    @msg_body = msg_body
    @msg_footer = msg_footer
    mail(to: 'coordination@cfeb-france.org', subject: object)
  end
end
