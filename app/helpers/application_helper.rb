require "net/http"

module ApplicationHelper

  def bootstrap_label_for(boolean_var, true_label='success', false_label='default')
    #default primary success info warning danger
    content_tag(:div, boolean_var, class:  boolean_var ? "label label-#{true_label}" : "label label-#{false_label}")
  end

  def bootstrap_badge_for(number)
    #default primary success info warning danger
    return content_tag(:div, number, class: "badge") if number <= 0
    return content_tag(:div, number, class: "badge badge-info") if number  < 10
    return content_tag(:div, number, class: "badge badge-success") if number  < 50
    return content_tag(:div, number, class: "badge badge-warning") if number  < 100
    return content_tag(:div, number, class: "badge badge-error") if number  < 200
    return content_tag(:div, number, class: "badge badge-inverse") if number  >= 200
  end

  def app_title(title)
    @title = title.blank? ? Settings.application_name : "CFEB-France - Identification | #{title}"
  end

  def app_logo(options = { size: 167 })
    link_to image_tag('frontend/logo-anrf.png', alt: "logo", class: 'logo', size: options[:size]), root_path
  end

  def admin_user
    redirect_to root_url, alert: t('controllers.actions.no_access_to') unless current_user.is_admin? #current_user.try(:admin)
  end


  def license_agreement
    cookies[:license_agreement] || false
  end

  def set_license_agreement
    cookies.permanent[:license_agreement] = true
  end

  def unset_license_agreement
    cookies.delete(:license_agreement)
  end

  def check_license_agreement
    unless license_agreement
      redirect_to conditions_path, :flash => { :warning => "Vous devez lire et accepter les conditions générales d'utilisation avant de continuer." }
    end
  end

  def admin_user?
    #current_user.try(:admin)
    current_user.is_admin?
  end

  def user_has_access_to_admin_panel?
    current_user.has_any_role? :admin, :supervisor
  end

  def user_roles_tag(user)
    user.roles.each do |role|
      concat(content_tag(:div, role.name, class: "badge") + " ")
    end
    return
  end

  def signed_in?
    !current_user.nil?
  end

  def current_user?(user)
    user == current_user
  end

  def role_adder(role, user)
    if params[role] == "1"
      user.grant(role)
    elsif params[role] == "0"
      user.remove_role(role)
    end
  end

  def link_to_sortable(column, title = nil, type = 'alpha')
    title ||= column.titleize
    type ||= sort_column_type
    css_class = (column.to_sym == sort_column.to_sym) ? "current #{sort_direction}" : nil
    direction = (column.to_sym == sort_column.to_sym && sort_direction.to_sym == :asc) ? :desc : :asc
    link_to custom_title(title, css_class, type), {:sort => column, :direction => direction, :type => type}, {:class => css_class}
  end

  def url_exist?(url_string)
    begin
      url = URI.parse(url_string)
      req = Net::HTTP.new(url.host, url.port)
      res = req.request_head(url.path)
      return true if res.code == "200"
    rescue => e
    end
    return false
  end

  private

  def custom_title(title, css_class, type)
    return  title if css_class.nil?
    if type.to_sym == :default
      icone = sort_direction.to_sym == :asc ? "sort-asc" : "sort-desc"
    else
      icone = sort_direction.to_sym == :asc ? "sort-#{sort_column_type}-asc" : "sort-#{sort_column_type}-desc"
    end
    fa_icon(icone, text: title, right: true)
  end

end
