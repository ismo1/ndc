module FlashesHelper
  def user_facing_flashes
    #flash.to_hash.slice("alert", "error", "notice", "success")
    flash.each do |msg_type, message|
      concat(content_tag(:div, message, class: "alert #{bootstrap_class_for(msg_type)} fade in") do
              concat content_tag(:button, '×'.html_safe, class: "close", data: { dismiss: 'alert' })
              concat message
            end)
    end
    nil
  end

  def bootstrap_class_for(flash_type)
      { success: "alert-success",
        error: "alert-error",
        alert: "alert-warning",
        notice: "alert-info"
      }[flash_type] || flash_type.to_sym
    end

end

