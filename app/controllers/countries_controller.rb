class CountriesController < ApplicationController
  include ApplicationHelper
  helper_method :sort_column, :sort_direction, :sort_column_type

  before_action :set_country, only: [:show, :edit, :update, :destroy, :destroy_flag]

  before_action :authenticate_user!
  #before_action :admin_user, only: [:index, :show, :edit, :update, :destroy, :destroy_flag]
  load_and_authorize_resource

  # GET /countries
  def index
    app_title('All Countries')
    @countries = Country.order("#{sort_column} #{sort_direction}").page params[:page]
    authorize! :read, @countries
  end

  # GET /countries/1
  def show
    app_title(@country.name)
    @users = @country.users.order("created_at asc").page params[:page]
    @all_users = @country.users.order("created_at asc")
    @all_users = [current_user] if !current_user.is_admin?
    respond_to do |format|
      format.html
      @filename = "#{@country.name}-#{DateTime.now.strftime("%Y-%m-%d_%H-%M-%S")}"
      #format.csv { send_data @all_users.to_csv, :filename => "#{@filename}.csv", :disposition => 'inline', :type => "multipart/related" } #To dowloaded csv format directly
      format.csv do
        stream = @all_users.to_csv
        send_data(stream, :type=>"text/csv",:filename => "#{@filename}.csv")
      end


      format.xls do
        stream = render_to_string(:template=>"users/index" )
        send_data(stream, :type=>"text/xls",:filename => "#{@filename}.xls")
      end
    end
  end

  # GET /countries/new
  def new
    app_title('New Country')
    @country = Country.new
  end

  # GET /countries/1/edit
  def edit
    app_title('Edit Country')
  end

  # POST /countries
  def create
    @country = Country.new(country_params)

    if @country.save
      redirect_to @country, notice:  t('controllers.actions.create', :ressource => t('mongoid.models.country.one', :default => "Country"))
    else
      render :new
    end
  end

  # PATCH/PUT /countries/1
  def update
    if @country.update_attributes(country_params)
      redirect_to @country, notice:  t('controllers.actions.update', :ressource => t('mongoid.models.country.one', :default => "Country"))
    else
      render :edit
    end
  end

  # DELETE /countries/1
  def destroy
    if @country.users.empty?
      @country.flag = nil
      @country.destroy
      redirect_to countries_url, notice: t('controllers.actions.destroy', :ressource => t('mongoid.models.country.one', :default => "Country"))
    else
      redirect_to request.referrer, alert: t('controllers.actions.destroy_restrict', :ressource => t('mongoid.models.country.one', :default => "Country"))
    end
  end

  # DELETE /countries/1
  def destroy_flag
    @country.change_flag
    redirect_to @country, notice: t('controllers.actions.destroy_flag')
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_country
      @country = Country.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def country_params
      params.require(:country).permit(:name, :flag, :indicatif, :enable)
    end

    def sort_column
      Country.fields.keys.include?(params[:sort]) ? params[:sort] : "name"
    end

    def sort_direction
      %w[asc desc].include?(params[:direction]) ?  params[:direction] : "asc"
    end

    def sort_column_type
      %w[alpha numeric amount].include?(params[:type]) ?  params[:type] : "alpha"
    end
end
