class RegistrationsController < Devise::RegistrationsController
  include ApplicationHelper

  before_action :check_license_agreement, only: [:new]
  def new
    set_license_agreement
    super
  end
end