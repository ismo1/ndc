class PagesController < ApplicationController
  include ApplicationHelper

  before_action :authenticate_user!, only: [:dashboard, :user_profil]
  before_action :set_user, only: [:user_profil, :user_edit_profil]
  before_action :correct_user, only: [:user_profil, :user_edit_profil]
  #before_action :admin_user, only: [:dashboard]

  def home
    app_title('Home')
  end

  def about
    app_title('About Us')
  end

  def conditions
    app_title("Conditions Générales d'utilisation")
  end

  def dashboard
    app_title('Admin')
    @countries = Country.where(enable: true).page params[:page]
  end

  def user_profil
    app_title(@user.pretty_name)
  end

  def user_edit_profil
    app_title('Edit')
  end

  def license_agreement
    if %w[false].include?(params[:terms])
      unset_license_agreement
      redirect_to(root_path)
    else
      set_license_agreement
      redirect_to(sign_up_path)
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @roles = User::ROLES
      @user = current_user
    end

    def correct_user
      redirect_to(request.referrer)  unless current_user == @user
    end

end
