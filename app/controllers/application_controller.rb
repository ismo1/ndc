class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :set_locale
  layout :layout_by_resource
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :set_edit_profile
  before_action :set_current_user

  #check_authorization


  rescue_from CanCan::AccessDenied do |exception|
    #redirect_to root_url, :alert => exception.message
    flash[:error] = "Access denied."
    redirect_to root_url
  end

  rescue_from Faraday::ConnectionFailed do |exception|
    #redirect_to root_url, :alert => exception.message
    flash[:error] = "Search engine's service is not available."
  end



  rescue_from CanCan::AuthorizationNotPerformed do |exception|
    flash.now[:error] = exception.message
  end

  def set_locale
    unless params[:locale]
      params[:locale] = extract_locale_from_accept_language_header
    end
    logger.debug "* Accept-Language: #{params[:locale]}"
    @available = %w(fr en)
    if @available.include? params[:locale]
      I18n.locale = params[:locale]
    elsif default_locale.nil?
      flash.now[:warning] = "'#{params[:locale].capitalize}' translation not available. '#{I18n.default_locale.capitalize}' will be set as default language."
      logger.error flash.now[:notice]
      I18n.locale = I18n.default_locale
      set_default_locale
    end
    logger.debug "* Locale set to '#{I18n.locale}'"
  end

  unless Rails.application.config.consider_all_requests_local #&& !Rails.env.development?
    rescue_from Exception, :with => :render_error
    #rescue_from ActionView::MissingTemplate, :with => :render_error
    rescue_from ActiveRecord::RecordNotFound, :with => :record_not_found
    rescue_from Mongoid::Errors::DocumentNotFound, :with => :record_not_found
    rescue_from ActionController::RoutingError, :with => :render_not_found
  end

  def record_not_found
    #render text: "404 Not Found", status: 404
    flash[:error] = "Record not Found or You don't have access to this section."
    redirect_to :root
  end

  #called by last route matching unmatched routes.
  #Raises RoutingError which will be rescued from in the same way as other exceptions.
  def raise_not_found!
    raise ActionController::RoutingError.new("No route matches #{params[:unmatched_route]}")
  end


  def set_edit_profile
    @edit_profile = false
    @edit_profile =  true if %w(registrations users pages).include?(controller_name) &&  %w(update edit user_edit_profil).include?(action_name)
    # @edit_profile = current_page?(:action => 'edit') ? true : false
  end

  def set_current_user
    User.current = signed_in? ? current_user : nil
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_in) { |u| u.permit(:username, :email, :remember_me, :password) }
    devise_parameter_sanitizer.for(:sign_up) { |u| u.permit({ roles: [] }, :country_id, :first_name, :last_name,
                                                            :email, :confirmed_at, :password, :password_confirmation,
                                                            :agree_terms, :city, :zip_code, :adresse, :phone, :civility) }
  end


  def layout_by_resource
    return 'page_layout' if devise_controller?
    case controller_name
    when 'pages'
      action_name == 'dashboard' ? 'application' : 'page_layout'
    else
      'application'
    end
  end

  private
  def extract_locale_from_accept_language_header
    request.env['HTTP_ACCEPT_LANGUAGE'].scan(/^[a-z]{2}/).first
  end

  #render 500 error
  def render_error(e)
    respond_to do |f|
      f.html{ render :template => "errors/500", :status => 500 }
      f.js{ render :partial => "errors/ajax_500", :status => 500 }
    end
  end

  #render 404 error
  def render_not_found(e)
    respond_to do |f|
      f.html{ render :template => "errors/404", :status => 404 }
      f.js{ render :partial => "errors/ajax_404", :status => 404 }
    end
  end

  def mobile_device?
    request.user_agent =~ /Mobile|webOS/
  end
  helper_method :mobile_device?

  def default_locale
    cookies[:default_locale]
  end
  helper_method :default_locale

  def set_default_locale
    cookies.permanent[:default_locale] = I18n.default_locale
  end
  helper_method :set_default_locale

  def unset_default_locale
    cookies.delete(:default_locale)
  end
  helper_method :unset_default_locale


end
