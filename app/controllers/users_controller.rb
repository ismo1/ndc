class UsersController < ApplicationController
  include ApplicationHelper

  helper_method :sort_column, :sort_direction, :sort_column_type

  before_action :set_user, only: [:show, :edit, :update, :destroy, :update_roles, :correct_user]
  before_action :authenticate_user!, only: [:index, :edit, :update, :destroy]

  load_and_authorize_resource

  #before_action :correct_user, only: [:edit, :update]
  #before_action :admin_user,     only: [:destroy, :index]

  # GET /users
  def index
    app_title('All users')
    @elasticsearch_service = Service.service('ELASTICSEARCH')
    if params[:query].present?
      begin
        @users = User.search_query(params[:query], params[:page])
      rescue => e
       @elasticsearch_service.down
        @users = User.order("#{sort_column} #{sort_direction}").page params[:page]
        flash.now[:error] = "Search engine's service is not available. This bug will be report to the administrator. Thanks!"
      end
    else
      @users = User.order("#{sort_column} #{sort_direction}").page params[:page]
    end
    @all_users = User.order("#{sort_column} #{sort_direction}")
    @all_users = [current_user] if !current_user.is_admin?
    respond_to do |format|
      format.html
      format.js
      #authorize! :show, @project
      @filename = "users-#{DateTime.now.strftime("%Y-%m-%d_%H-%M-%S")}"
      #format.csv { send_data @all_users.to_csv, :filename => "#{@filename}.csv", :disposition => 'inline', :type => "multipart/related" } #To dowloaded csv format directly
      format.csv do
        stream = @all_users.to_csv
        send_data(stream, :type=>"text/csv",:filename => "#{@filename}.csv")
      end

      #format.xls { send_data @all_users.to_csv(col_sep: "\t") } #To dowloaded xls format directly
      #format.xls { headers["Content-Disposition"] = "attachment; filename=\"#{@filename}.xls\"" } #To dowloaded xls format by pass on view
      format.xls do
        stream = render_to_string(:template=>"users/index" )
        send_data(stream, :type=>"text/xls",:filename => "#{@filename}.xls")
        #, notice:  t('controllers.actions.create', :ressource => t('mongoid.models.user.one', :default => "User"))
      end
    end
  end

  # GET /users/1
  def show
    app_title(@user.pretty_name)
  end

  # GET /users/new
  def new
    app_title('New user')
    @user = User.new
  end

  # GET /users/1/edit
  def edit
    app_title('Edit user')
  end

  def sign_up
    @title = app_title('Sign up')
    @user = User.new
  end


  # POST /users
  def create
    @user = User.new(user_params)

    if @user.save
      UserMailer.welcome_email(@user).deliver_later
      redirect_to admin_user? ? @user : user_profil_path, notice:  t('controllers.actions.create', :ressource => t('mongoid.models.user.one', :default => "User"))
    else
      render :new
    end
  end

  # PATCH/PUT /users/1
  def update
    if @user.update_attributes(user_params)
      # UserMailer.welcome_email(@user).deliver_later(wait: 1.minute)
      redirect_to admin_user? ? @user : user_profil_path, notice:  t('controllers.actions.update', :ressource => t('mongoid.models.user.one', :default => "User"))
    else
      render :edit
    end
  end

  # PATCH/PUT /users/1
  def update_roles
    @roles.each { |role| role_adder(role, @user) }
    if @user.save
      redirect_to admin_user? ? @user : user_profil_path, notice:  t('controllers.actions.update_roles', :ressource => t('mongoid.models.user.one', :default => "User"))
    else
      render :show
    end
  end

  # DELETE /users/1
  def destroy
    @user.destroy
    redirect_to users_url, notice:  t('controllers.actions.destroy', :ressource => t('mongoid.models.user.one', :default => "User"))
  end

  def autocomplete
    render json: User.search(params[:query], fields: [{first_name: :text_start}], limit: 10).map(&:first_name)
    #render json: User.search(params[:query], fields: [{first_name: :text_start}, {last_name: :text_start}, {email: :text_start}, {city: :text_start}, {zip_code: :text_start}], operator: "or", limit: 10).map(&:name)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @roles = User::ROLES
      @user = User.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def user_params
      params.require(:user).permit(:first_name, :last_name, :email, :admin, :country_id, :password, :password_confirmation, :agree_terms, :city, :zip_code, :adresse, :phone, :civility)
    end

    def correct_user
      redirect_to(request.referrer)  unless current_user == @user
    end

    def sort_column
      User.fields.keys.include?(params[:sort]) ? params[:sort] : "country_id"
    end

    def sort_direction
      %w[asc desc].include?(params[:direction]) ?  params[:direction] : "asc"
    end

    def sort_column_type
      %w[alpha numeric amount].include?(params[:type]) ?  params[:type] : "alpha"
    end

end
