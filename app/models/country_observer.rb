class CountryObserver < Mongoid::Observer

  include SessionInfosConcern

  def after_create(country)
    object="Country Notification - #{country.name} added!"
    msg_head="Hi Admin!"
    msg_body="New country (#{country.id}) was added! #{DateTime.now.strftime("On %A, %m %B %Y at %H:%M %p")}."
    msg_footer="Thanks"
    msg_footer="That is done  by #{session_user_info}."
    Rails.logger.warn("#{msg_body} || #{msg_footer}")
    NotifierMailer.notify_admin(object, msg_head, msg_body, msg_footer).deliver
  end

  def after_destroy(country)
    object="Country Notification - #{country.name} destroyed"
    msg_head="Hi Admin!"
    msg_body="The country '#{country.name}' was destroyed! #{DateTime.now.strftime("On %A, %m %B %Y at %H:%M %p")}."
    msg_footer="Thanks"
    msg_footer="That is done  by #{session_user_info}."
    Rails.logger.warn("#{msg_body} || #{msg_footer}")
    NotifierMailer.notify_admin(object, msg_head, msg_body, msg_footer).deliver
  end

  def after_update(country)
    return unless country.changed?
    object="Country Notification - '#{country.name}' was updated!"
    msg_head="Hi Admin!"
    msg_body="The country '#{country.name}' was updated! #{DateTime.now.strftime("On %A, %m %B %Y at %H:%M %p")}. \n Changed attributes were :\n #{country.changes}"
    msg_footer="Thanks"
    msg_footer="That is done  by #{session_user_info}."
    Rails.logger.warn("#{msg_body} || #{msg_footer}")
    NotifierMailer.notify_admin(object, msg_head, msg_body, msg_footer).deliver
  end
end