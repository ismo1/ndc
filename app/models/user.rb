class User
  include Mongoid::Document
  include Mongoid::Timestamps
  include Columns
  include GlobalID::Identification

  #Elastic Search Model
  searchkick
  #searchkick autocomplete: ['term'], text_start: ['term'], suggest: ["term"]


  #role
  rolify

  #adding role behaviour
  #resourcify

  belongs_to :country #, :class_name => 'Country'
#  after_commit :reindex_country


  field :first_name, type: String
  field :last_name, type: String
  field :email, type: String
  field :agree_terms, type: Mongoid::Boolean
  field :city, type: String
  field :zip_code, type: String
  field :phone, type: String
  field :adresse, type: String
  field :civility, type: String

  field :admin, type: Mongoid::Boolean

  index({ city: 1  })


  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  # :async,
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable

  ## Database authenticatable
  field :email,              type: String, default: ""
  field :encrypted_password, type: String, default: ""

  ## Recoverable
  field :reset_password_token,   type: String
  field :reset_password_sent_at, type: Time

  ## Rememberable
  field :remember_created_at, type: Time

  ## Trackable
  field :sign_in_count,      type: Integer, default: 0
  field :current_sign_in_at, type: Time
  field :last_sign_in_at,    type: Time
  field :current_sign_in_ip, type: String
  field :last_sign_in_ip,    type: String

  ## Confirmable
  field :confirmation_token,   type: String
  field :confirmed_at,         type: Time
  field :confirmation_sent_at, type: Time
  field :unconfirmed_email,    type: String # Only if using reconfirmable

  ## Lockable
  # field :failed_attempts, type: Integer, default: 0 # Only if lock strategy is :failed_attempts
  # field :unlock_token,    type: String # Only if unlock strategy is :email or :both
  # field :locked_at,       type: Time

  validates :first_name, :last_name, presence: :true, :length => {:minimum => 3, :maximum => 30}
  validates :city, :zip_code, presence: :true, :on => :create, :length => {:minimum => 3, :maximum => 30}
  validates :email, presence: true

  validates :country, presence: { message: ": Vous devez choisir un pays"}

  VALID_CIVILITIES = %w(Mme Mrs. Mr Mr.)
  validates_inclusion_of :civility, :in => VALID_CIVILITIES, :message => ": Vous devez choisir une civilité valide", :on => :create


  # validates :agree_terms, acceptance: true
  validates :agree_terms, :presence => true, :on => :create

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(?:\.[a-z\d\-]+)*\.[a-z]+\z/i
  validates :email, presence: true, format: { with: VALID_EMAIL_REGEX  }, :uniqueness => { case_sensitive: false  }
  validates :password, length: { minimum: 6  }, :on => :create

  #callbacks
  before_save :down_upcase_attributes


  #pagination
  paginates_per 10

  #scopes
  scope :confirmed, ->{ not where(:confirmed_at => nil) }
  scope :unconfirmed, ->{ where(:confirmed_at => nil) }
  scope :country_sort, ->{ asc(:country_id)  }

  ROLES = [:admin, :supervisor, :simpleuser]

  def down_upcase_attributes
    self.email = email.downcase
    self.first_name = first_name.upcase
    self.last_name = last_name.capitalize
  end

  def pretty_name
    "#{self.civility} #{self.first_name} #{self.last_name}"
  end

  def self.current
    Thread.current[:user]
  end

  def self.current=(user)
    Thread.current[:user] = user
  end

  def self.to_csv(options = {})
    CSV.generate(options) do |csv|
      column_names = fields.keys
      csv << column_names
      all.each do |user|
        csv << user.attributes.values_at(*column_names)
      end
    end
  end

  def search_data
    as_json only: [:first_name, :last_name, :email, :city, :zip_code, :phone, :adresse, :civitlity, :country]
    # or equivalently
    {
      first_name: first_name,
      last_name: last_name,
      email: email,
      city: city,
      zip_code: zip_code,
      phone: phone,
      adresse: adresse,
      civility: civility,
      country: country
    }
  end

  def self.search_query(param, page)
      User.search "%#{param}%", suggest: true, misspellings: {distance: 2}, page: page, per_page: 10
  end

#  def reindex_country
#    country.reindex # or reindex_async
#  end

end
