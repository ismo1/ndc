class ServiceObserver < Mongoid::Observer
  include SessionInfosConcern

  def after_update(service)
    if service.status != true
      object="Service Notification - #{service.name} is down or not avalable"
      msg_head="Hi Admin!"
      msg_body="The service (#{service.name}) was donwned or not avalable! #{DateTime.now.strftime("On %A, %d %B %Y at %H:%M %p")}."
      msg_footer= object
      Rails.logger.warn("#{msg_body} || #{msg_footer}")
      NotifierMailer.notify_admin(object, msg_head, msg_body, msg_footer).deliver
    end
  end

end