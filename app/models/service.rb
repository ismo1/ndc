class Service
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paperclip
  include Columns

  field :name, type: String
  field :descriotion, type: String
  field :status, type: Mongoid::Boolean

  validates :name,
            presence: true,
            uniqueness: { message: "Un service du même nom existe déjà"}
  validates_length_of :name, :in => 2..64
  VALID_SERVICES = %w(ELASTICSEARCH REDIS WORKER)
  validates_inclusion_of :name, :in => VALID_SERVICES, :message => ": Vous devez choisir un de service valide", :on => :create

  def self.service(name)
    where(:name => name).first
  end

  def up
    status = true
    save
  end

  def down
    status = false
    save
  end
end
