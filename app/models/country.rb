class Country
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paperclip
  include Columns

  #adding role behaviour
  resourcify

  has_many :users, dependent: :restrict

  field :_id, type: String, default: ->{ name }
  field :name
  #field :name, type: String
  #field :flag, type: String
  field :indicatif, type: Integer
  field :enable, type: Mongoid::Boolean, default: false

  has_mongoid_attached_file :flag, :styles => {
                                                :thumb => '50x50',
                                                :small    => '100x100#',
                                                :medium   => '250x250',
                                                :large    => '500x500>'
                                              },
                                                :default_url => "missing/:style/missing-image.png",
                                                :default_style => :medium,
                                                :url => "/system/:class/:attachment/:id/:style/:basename.:extension",
                                                :path => ":rails_root/public/system/:class/:attachment/:id/:style/:basename.:extension"

  index({ name: 1  })


  validates_attachment_content_type :flag, :content_type => /\Aimage\/.*\Z/
  validates :name,
            presence: true,
            uniqueness: { message: "Ce nom existe déjà"}
  validates_length_of :name, :in => 2..25
  validates :indicatif,
            presence: true,
            uniqueness: { message: "un autre pays possède dejà ce t'indicatif"},
            :inclusion => { in: 1..999, :message => "Merci de saisir une valeur entre 1 et 999" }


  #pagination
  paginates_per 10

  #scopes
  # asc(:name)
  scope :indicatif_sort, ->{ asc(:indicatif)  }
  scope :name_sort, ->{ where(:enable => true).asc(:name) }

  def change_flag
    flag = nil
    save
    file = File.open(Rails.root.join('app', 'assets', 'images', 'missing','medium' ,'missing-image.png'))
    flag = file
    file.close
    save
  end

  def users_count
    users.count
  end

end
