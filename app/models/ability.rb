class Ability
  include CanCan::Ability

  def initialize(user)
  	alias_action :create, :read, :update, :destroy, :destroy_flag, :to => :crud
    user ||= User.new
    if user.is_admin?
    	can :manage, :all
    elsif user.is_supervisor?
    	#supervisor
      can :read, :all
      can [:read, :update], User do |load_user|
        load_user.try(:_id) == user.id
      end
      #can :manage, User
      #can :manage, Country
		else #if user.is_simpleuser? || user.roles.empty?
      #default
      #cannot :read, :all
      #cannot :crud, Country
      #cannot :crud, User
      #can [:read, :update], User, :_id => user.id
      can [:read, :update], User do |load_user|
        load_user.try(:_id) == user.id  #|| user.has_role?(:supervisor)
      end
    end

    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities

  	#can do all action withaout destroy if this cambinaison is used
		#can :manage, Project
		#cannot :destroy, Project

		#can :read, Forum
  	#can :write, Forum, :id => Forum.with_role(:moderator, user).pluck(:id)

  end
end
