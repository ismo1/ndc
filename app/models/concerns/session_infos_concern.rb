module SessionInfosConcern

  extend ActiveSupport::Concern

  module ClassMethods
  end

  def session_user_info
    User.current.nil? ? nil : "#{User.current.try(:pretty_name)} (#{User.current.try(:email)})"
  end

end