class UserObserver < Mongoid::Observer
  include SessionInfosConcern

  def after_save(user)
    NotifierMailer.send_identification_confirmation(user).deliver if user.confirmed_at_changed?
  end

  def after_destroy(user)
    object="User Notification - #{user.pretty_name} destroyed!"
    msg_head="Hi Admin!"
    msg_body="The user (#{user.pretty_name}) with id '#{user.id}' was destroyed! #{DateTime.now.strftime("On %A, %d %B %Y at %H:%M %p")}."
    msg_footer= session_user_info.nil? ? nil : "That is done  by #{session_user_info}."
    Rails.logger.warn("#{msg_body} || #{msg_footer}")
    NotifierMailer.notify_admin(object, msg_head, msg_body, msg_footer).deliver
  end

  def after_create(user)
    object="User Notification - #{user.pretty_name} was added!"
    msg_head="Hi Admin!"
    msg_body="The user (#{user.pretty_name}) was signup successfully! #{DateTime.now.strftime("On %A, %d %B %Y at %H:%M %p")}."
    msg_footer= session_user_info.nil? ? nil : "That is done  by #{session_user_info}."
    Rails.logger.warn("#{msg_body} || #{msg_footer}")
    NotifierMailer.notify_admin(object, msg_head, msg_body, msg_footer).deliver
  end

end