# config valid only for current version of Capistrano
# set :stages, ["staging", "production"]
# set :default_stage, "production"

lock '3.4.0'

set :application, 'ndc'
set :repo_url, 'git@bitbucket.org:ismo1/ndc.git'

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
# set :deploy_to, '/var/www/my_app_name'
set :deploy_to, "/var/apps/www/plateforme"

# Default value for :scm is :git
set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
set :pty, true

# which config files should be copied by deploy:setup_config
# see documentation in lib/capistrano/tasks/setup_config.cap
# for details of operations
set(:config_files, %w(database.yml secrets.yml mongoid.yml settings.yml schedule.rb smtp.rb .env ))

# Default value for :linked_files is []
set :linked_files, fetch(:linked_files, []).push('config/database.yml', 'config/secrets.yml', 'config/mongoid.yml', 'config/settings.yml', 'config/schedule.rb', 'config/smtp.rb', '.env')

# Default value for linked_dirs is []
set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'tmp/restart.txt', 'vendor/bundle', 'public/system')
# set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system')

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
set :keep_releases, 3


namespace :deploy do

  after :restart, :clear_cache do
    on roles(:app), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end

  desc "Precompile assets locally and then rsync to deploy server"
  task :assets_precompile do
  on roles(:app) do
    run("cd #{deploy_to}/current && /usr/bin/env rake `assets:precompile` RAILS_ENV=production")
  end
end


  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      #Your restart mechanism here, for example:
      execute :touch, release_path.join('tmp/restart.txt')
    end
  end

    # make sure we're deploying what we think we're deploying
    before :deploy, "deploy:check_revision"
    # only allow a deploy with passing tests to deployed
    #before :deploy, "deploy:run_tests"
    # compile assets locally then rsync
    #after 'deploy:symlink:shared', 'deploy:compile_assets_locally'
    after 'deploy:symlink:shared', 'deploy:assets_precompile'
    after :finishing, 'deploy:cleanup'

    # remove the default nginx configuration as it will tend
    # to conflict with our configs.
    # before 'deploy:setup_config', 'nginx:remove_default_vhost'

    # reload nginx to it will pick up any modified vhosts from
    # setup_config
    #  after 'deploy:setup_config', 'nginx:reload'

    # Restart monit so it will pick up any monit configurations
    # we've added
    #  after 'deploy:setup_config', 'monit:restart'

    # As of Capistrano 3.1, the `deploy:restart` task is not called
    # automatically.
    after 'deploy:publishing', 'deploy:restart'
  end

end
