# SMTP_SETTINGS = {
#   address: ENV.fetch("SMTP_ADDRESS"), # example: "smtp.sendgrid.net"
#   authentication: :plain,
#   domain: ENV.fetch("SMTP_DOMAIN"), # example: "heroku.com"
#   enable_starttls_auto: true,
#   password: ENV.fetch("SMTP_PASSWORD"),
#   port: "587",
#   user_name: ENV.fetch("SMTP_USERNAME")
# }

SMTP_SETTINGS = {
  :user_name => ENV["SMTP_USERNAME"],
  :password => ENV["SMTP_PASSWORD"],
  :domain => 'www.plateforme.cfeb-france.org',
  :address => 'smtp.sendgrid.net',
  :port => 587,
  :authentication => :plain,
  :enable_starttls_auto => true
 }
