Rails.application.routes.draw do
  resources :services
 # devise_for :users
  resources :users
  resources :countries

  get '/destroy_flag/:id', to: 'countries#destroy_flag', as: 'destroy_flag'
  patch '/update_roles/:id', to: 'users#update_roles', as: 'update_roles'

  #root :to => "countries#index"

  #pages high_voltage
  # scope "/:locale", locale: /en|fr/ do
  #   get "/pages/:id" => 'high_voltage/pages#show', :as => :page, :format => false
  # end

  get "/home" => 'pages#home'
  get "/about" => 'pages#about'
  get "/conditions" => 'pages#conditions'
  get "/dashboard" => 'pages#dashboard'
  get "/user_profil" => 'pages#user_profil'
  get "/user_edit_profil" => 'pages#user_edit_profil'
  get "/license_agreement" => 'pages#license_agreement'
  get "/autocomplete" => 'users#autocomplete'


  root :to => 'pages#home'
  #devise

  devise_for :users , path: "auth", path_names: {
                                                  sign_in: 'login',
                                                  sign_out: 'logout',
                                                  password: 'secret',
                                                  confirmation: 'verification',
                                                  unlock: 'unblock',
                                                  registration: 'register',
                                                  sign_up: 'sign_up'
                                                }, :controllers => {:registrations => "registrations"}
  devise_scope :user do
    get     "/register",      to:   "registrations#new",          as: 'sign_up'
    get     "/login",         to:   "devise/sessions#new",        as: 'sign_in'
    get     "/logout",        to:   "devise/sessions#destroy",    as: 'sign_out'
  end

  get     '/sign_up' => redirect('/register')
  get     '/auth/register' => redirect('/register')
  get     '/sign_in' => redirect('/login')
  get     '/sign_out' => redirect('/logout')


  # get '*unmatched_route', :to => 'application#render_not_found'
  get ':not_found' => redirect('/'), :constraints => { :not_found => /.*/ }
end
