# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


  countries = [['France', 33], ['Espagne', 34], ['Portugal', 351], ['Angleterre', 44]]
  countries.map do |name, indicatif|
    Country.create(_id: name, name: name, indicatif: indicatif, enable: true)
  end
